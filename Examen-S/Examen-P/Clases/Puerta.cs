﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Examen_P.Clases
{
   public class Puerta
    {
        public string Tamaño { get; set; }
        public string Color { get; set; }
        public string Diseño { get; set; }

        public Puerta()
        {
            this.Tamaño = "Grande";
            this.Color = "Roja";
            this.Diseño = "Bonito";
        }
        public void mostrar (string Propiedad)
        {
            this.Tamaño = "Propiedad";
            this.Color = "Propiedad";
            this.Diseño = "Propiedad";
        }
    }
}

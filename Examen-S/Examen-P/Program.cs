﻿using Examen_P.Clases;
using System;

namespace Examen_P
{   
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("\n\n************************************Bienvenido a mi Examen*************************************\n\n");
            Console.WriteLine("\n\n Datos de Puerta");
            Puerta Miguel = new Puerta();
            Miguel.Tamaño = "Grande, Mediana, Pequeña";
            Miguel.Color = "Rija, Café, Blanca";
            Miguel.Diseño = "Bonita";
            Console.WriteLine("El tamaño de la Puerta es:"+Miguel.Tamaño);
            Console.WriteLine("El Color de la Puerta es:"+Miguel.Color);
            Console.WriteLine("El diseño de la Puerta es:"+Miguel.Diseño);

            Console.WriteLine("\n\n Datos de la Puerta de Madera");
            Puerta_de_Madera Angel = new Puerta_de_Madera();
            Angel.Tamaño = "Grande";
            Angel.Color = "Roja, Café, Blanca";
            Angel.Diseño = "Gama alta";
            Angel.PrecioBase = "200";
            Angel.Preciodeseguro = "100";
            Console.WriteLine("El tamaño de la Puerta es:" + Angel.Tamaño);
            Console.WriteLine("El Color de la Puerta es:" + Angel.Color);
            Console.WriteLine("El diseño de la Puerta es:" + Angel.Diseño);
            Console.WriteLine("El Precio base de la puerta es:"+Angel.PrecioBase);
            Console.WriteLine("El precio de Seguro de la Puerta es:"+Angel.Preciodeseguro);
            
            {
                int PB = 20; int CS = 1; int PS = 100;
                int Precio = CS * PS;
                int Producto = PB + Precio;
                Console.WriteLine("El Calculo del precio Construir es:" + Producto);
            }


            Console.WriteLine("\n\n Dato de la puerta Hierro");
            Puerta_de_Hierro Cj = new Puerta_de_Hierro();
            Cj.Tamaño = "Mediana";
            Cj.Color = "Café";
            Cj.Diseño = "Gama media";
            Cj.Porcentaje_del_Impuesto = "12%";
            Cj.Precio_de_Puerta = "200";
            Console.WriteLine("El tamaño de la Puerta es:" + Cj.Tamaño);
            Console.WriteLine("El Color de la Puerta es:" + Cj.Color);
            Console.WriteLine("El diseño de la Puerta es:" + Cj.Diseño);
            Console.WriteLine("El porcentaje del Impuesto es:"+Cj.Porcentaje_del_Impuesto);
            Console.WriteLine("El precio de la puerta es:"+Cj.Precio_de_Puerta);
            Console.WriteLine("Calculo_de_Precio_construir");
            {
                int Precio_de_Puerta = 200; int Porcentaje_de_Impuesto = 12;
                int Operacion = (Precio_de_Puerta * Porcentaje_de_Impuesto) / 100;
                
                Console.WriteLine("El Calculo del precio Construir es:"+ Operacion);
            }


            Console.WriteLine("\n\n Dato de la Aluminio ");
            Puerta_de_Aluminio Gta = new Puerta_de_Aluminio();
            Gta.Tamaño = "Pequeña";
            Gta.Color = "Blanco";
            Gta.Diseño = "Gama Media";
            Gta.Precio_Fijo = "200";
            Gta.Precio_de_Vidrio = "50";
            Console.WriteLine("El tamaño de la Puerta es:" + Gta.Tamaño);
            Console.WriteLine("El Color de la Puerta es:" + Gta.Color);
            Console.WriteLine("El diseño de la Puerta es:" + Gta.Diseño);
            Console.WriteLine("El precio de la puerta es:"+Gta.Precio_Fijo);
            Console.WriteLine("El precio del vidrio es:"+Gta.Precio_de_Vidrio);
            
            {
                int PF = 200;
                int PV = 50;
                int Operacion = PF + PV;
                Console.WriteLine("El Calculo del precio Construir es:" + Operacion);
            }

            Console.ReadLine();
            Console.ReadKey();


        }
    }
}
